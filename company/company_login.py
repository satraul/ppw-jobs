from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Company
from .company_data import get_company_data, get_company_id
import requests, json

CLIENT_ID = "866hc1so0n9js9"
CLIENT_SECRET = "IFSny4QeaB0XYT8D"
# REDIRECT_URI = "http://ppw-jobs.herokuapp.com/company/request-token/"
REDIRECT_URI = "http://ppw-jobs.herokuapp.com/company/request-token/"
response = {}

def request_auth(request: HttpRequest):
    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile r_emailaddress rw_company_admin w_share")
    return redirect(uri)

def request_token(request: HttpRequest):
    request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"

    if request.method == "GET":
        code = request.GET['code']
        print("code = " + code)
        state = request.GET['state']
        request_data = {
            "grant_type":"authorization_code",
            "code":code,
            "redirect_uri":REDIRECT_URI,
            "client_id":CLIENT_ID,
            "client_secret":CLIENT_SECRET
        }

        request_header = {
            'content_type':"application/json"
        }
        response = requests.request("POST", request_token_uri, data=request_data,
                         headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))
        request.session['session-id'] = response_data['access_token']
        #  return HttpResponseRedirect('/profile/', reques)
        new_company_id = get_company_id(request)
        request.session['company_id'] = new_company_id
        company_data = get_company_data(request)
        is_exist = Company.objects.filter(companyID=new_company_id)
        if is_exist.count() == 0:
            specialties = ", ".join(company_data.get('specialties',{}).get('values',''))
            Company.objects.create(
                name=company_data.get('name', ''),
                company_type=company_data.get('companyType', {}).get('name', ''),
                web=company_data.get('websiteUrl', ''),
                description=company_data.get('description', ''),
                specialty=specialties,

                picture_url=company_data.get('squareLogoUrl', ''),
                id_linkedin=new_company_id,
                link_linkedin="www.google.com"
                # Belum bisa nyari ling linkedin nya
            )

        return redirect('/profile/')
    else:
        return redirect('/')

def auth_logout(request):
    request.session.flush() # menghapus semua session
    response.clear()
    return HttpResponseRedirect(reverse('login:index'))

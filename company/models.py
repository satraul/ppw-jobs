from django.db import models
from django.contrib.auth.models import User as DjangoUser


# Create your models here.

class Company(DjangoUser):
    """
    Description:
    Model for user app

    """

    # basic company
    name = models.CharField(max_length=140)
    company_type = models.CharField(max_length=140)
    web = models.CharField(max_length=140)
    description = models.CharField(max_length=500)
    specialty = models.CharField(max_length=140)

    # linkedin
    picture_url = models.CharField(blank=True, max_length=300)
    id_linkedin = models.CharField(blank=True, max_length=20)
    link_linkedin = models.CharField(blank=True, max_length=100)

    lastseen_at = models.DateTimeField('Last Seen at', auto_now=True, editable=False)
    created_at = models.DateTimeField('Created at', auto_now_add=True, editable=False)

    def __str__(self):
        return self.name

    def set_company_data(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def get_name(self):
        return self.name

    #
    # def get_token_linkedin(self):
    #     return self.token_linkedin

    class Meta:
        ordering = ('name', 'web', 'company_type', 'description', 'specialty')

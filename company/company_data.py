from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Company
import requests, json

def get_profile_data(request):
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/people/~?oauth2_access_token="+token+"&format=json"
    data_profile = requests.request("GET", url=url)
    data_profile = json.loads(data_profile.content.decode('utf8'))
    print(data_profile)
    return data_profile

def get_company_id(request):
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/companies?oauth2_access_token="+token+"&format=json&is-company-admin=true"
    company_id = requests.request("GET", url=url)
    company_id = json.loads(company_id.content.decode('utf8'))
    size = len(company_id['values'])
    company_id = str(company_id['values'][size-1]['id'])
    # Masih milih yang terkahir
    return company_id

def get_company_data(request):
    token = request.session['session-id']
    cpy_id = request.session['company_id']
    url = "https://api.linkedin.com/v1/companies/"+cpy_id+":(id,name,ticker,description,company-type,website-url,specialties,square-logo-url)?oauth2_access_token="+token+"&format=json"
    company_data = requests.request("GET", url=url)
    company_data = json.loads(company_data.content.decode('utf8'))
    return company_data

from django.conf.urls import url
from .company_login import request_token, request_auth, auth_logout
from .views import index, show_profile
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^authenticate/', request_auth, name='authenticate'),
    url(r'^request-token/', request_token , name='get_token'),
    url(r'^logout/', auth_logout , name='logout'),
]

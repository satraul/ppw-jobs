from django.shortcuts import render, redirect
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Company
import requests, json

response = []
def index(request):
    return render(request, 'testLogin.html', response)

def show_profile(request):
    if 'session-id' in request.session:
        company_data = get_company_data(request)
        print(company_data)
        response['companyName'] = company_data.get('name', '')
        response['companyType'] = company_data.get('companyType', {}).get('name', '')
        response['companyWeb'] = company_data.get('websiteUrl', '')
        specialties = ", ".join(company_data.get('specialties',{}).get('values',''))
        response['companySpecialty'] = specialties
        response['squareLogoUrl'] = company_data.get('squareLogoUrl', '')
        response['companyDescription'] = company_data.get('description', '')
    else:
        return redirect('/')
    return render(request, 'compProfile.html', response)

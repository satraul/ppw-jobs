from django.apps import AppConfig


class AppProfileConfig(AppConfig):
    name = 'user_profile'
